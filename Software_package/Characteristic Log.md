# 文档名称说明
特性日志，用于记录Kubeode迭代历程，后期会不断演进，尽请期待！
### 2022-06-19新增特性
1.全新版本kubeode, k8s版本升级v1.23.5

2.支持1-多台，多master高可用。

3.全程Tui终端图形化交互式安装。

### 2022-04-24新增特性 [新手推荐]
1.更新docker-ce等yum软件包到最新版。

2.修复centos7.8 7.6 7.9的兼容性

### 2021-12-20新增特性
1.更新docker-ce等软件包到最新版。

2.修复centos7.8的兼容性

### 2021-9-28新增特性

1.更新配套生态软件版本coredns-1.8.4 kubeapps-2.4.0  grafana-8.1.3  prometheus-2.26.0 ,新增k8s ui kuboard(非官方)

2.新增kubeapp  k8s  helmhub应用商店

3.新增ingress

4.更新离线镜像包

5.升级k8s版本v1.19.14

### 2021-9-29新增特性
1.更新镜像源.阿里的被删了更换为华为仓库
### 2020-12-25新增特性
1.默认k8s版本v1.19.5 纯二进制部署 非kubeadm

2.升级所有二进制相关组件
cfssl-certinfo_1.5.0_linux_amd64
cfssl_1.5.0_linux_amd64
cfssljson_1.5.0_linux_amd64
etcd-v3.4.14-linux-amd64.tar.gz
flannel-v0.13.0-linux-amd64.tar.gz
heketi-v10.1.0.linux.amd64.tar.gz
helm-v3.4.2-linux-amd64.tar.gz
v1.19.5_kubernetes-server-linux-amd64.tar.gz

3.完善多master高可用,基于ipvs

4.修复若干bug,线上环境已使用

### 2020-4-21新增特性
1.新增多master高可用,基于ipvs(仅供测试使用目前还没有深入测试过可能会有bug)
2.修复一些bug

### 2020-2-8新增特性

1.新增卸载功能,优化部署脚本
修复一些bug+内核优化
### 2019-10-19新增特性
1.修复一些bug+内核优化
### 2019-10-10新增特性
1.修复时区问题,所有pod默认中国上海时区
### 2019-9-16新增特性
1. 集群版新增prometheus  +grafan集群监控环境(被监控端根据集群数量自动弹性扩展)  默认端口30000，默认账户密码admin admin

2. 必须在启用数据持久化功能的基础上才会开启

3. grafana已内置了k8s集群pod监控模板,集群基础信息监控模板,已内置饼图插件，开箱即可用

### 2019-9-27新增特性
1. 单机版新增nfs_k8s动态存储,单机版k8s也能愉快的玩helm了,后续增加集群版nfs云端存储/本地存储持久化方案。

2. 单机版新增prometheus+grafan监控环境

3. 单机版默认storageclasses均为gluster-heketi
### 2019-9-25新增特性
1. 优化一些参数,集群版持久化功能支持最低2个节点起

2. 单机版新增helm

3. 修复一些bug
### 2019-9-13新增特性
1. 集群版新增coredns 感谢群内dockercore大佬的指导
2. 优化集群版部署脚本,新增集群重要功能监测脚本
3. 新增内置busybox镜像测试dns功能
### 2019-8-26新增特性
1. 新增node节点批量增删
2. 新增glusterfs分布式复制卷---持久化存储(集群版4台及以上自动内置部署)
### 2019-7-11新增特性
修复部分环境IP取值不精确导致etcd安装失败的问题
### 2019-7-10新增特性
1. 新增集群版 web图形化控制台dashboard
2. 更新docker-ce版本为 Version: 18.09.7
K8s集群版安装完毕,web控制界面dashboard地址为 http://IP:42345
### 2019-7-1新增特性
新增单机版 web图形化控制台dashboard
K8s单机版安装完毕,web控制界面dashboard地址为 http://IP:42345
# 回到Kubeode首页
[回到Kubeode首页](https://gitee.com/q7104475/kubeode?_from=gitee_search)